<?php

namespace App\Providers;
use Inertia\Inertia;
use App\Http\Controllers\Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
     // Inertia::share([
     //        'errors' => function () {
     //            return Session::get('errors')
     //                ? Session::get('errors')->getBag('default')->getMessages()
     //                : (object) [];
     //        },
     //    ]);
  
        // Inertia::share('flash', function () {
        //     return [
        //         'message' => Session::get('message'),
        //     ];
        // });
    }
}
