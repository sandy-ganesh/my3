<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddpawnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addpawns', function (Blueprint $table) {
            $table->id()->autoIncrement();
              $table->foreignId('RoleId')->references('id')->on('roles')->cascadeOnDelete();
              $table->string('Name');
              $table->integer('AmountProvider');
              $table->integer('RemainingAmount');
              $table->integer('Interest');
              $table->integer('TotalAmount');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addpawns');
    }
}
