<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_roles', function (Blueprint $table) {
             $table->id();
            $table->foreignId('RoleId')->references('id')->on('roles')->cascadeOnDelete();
            $table->foreignId('PermissionId')->references('id')->on('permissions')->cascadeOnDelete();
            // $table->timestamp('created_at')->nullable();
    
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_roles');
    }
}
