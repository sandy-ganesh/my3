<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class addpawns extends Model
{
    use HasFactory;
     protected $fillable = [
    	'ProductId','RoleId','Name','AmountProvider','RemainingAmount','Interest','TotalAmount',
    ];
}
