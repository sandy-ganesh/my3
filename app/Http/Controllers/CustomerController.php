<?php

namespace App\Http\Controllers;
use Session;


use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\customers;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $customers = Customer::latest()->paginate(10);

        return Inertia::render('customers/Index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('customers/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Post::create(
        //     Request::validate([
        //         'title' => ['required', 'max:90'],
        //         'description' => ['required'],
        //     ])
        // );

        // return Redirect::route('posts.index');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //  $customers = Product::find($id);
        // return response()->json($customers);
         // return view('categorys.show',compact('category'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  return Inertia::render('Post/Edit', [
        //     'post' => [
        //         'id' => $post->id,
        //         'title' => $post->title,
        //         'description' => $post->description
        //     ]
        // ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  $data = Request::validate([
        //         'title' => ['required', 'max:90'],
        //         'description' => ['required'],
        //     ]);
        // $post->update($data);
        

        // return Redirect::route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $post->delete();
        
        // return Redirect::route('posts.index');
    }
}
