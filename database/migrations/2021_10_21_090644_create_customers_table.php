<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
             $table->id()->autoIncrement();
            // $table->BigInteger('ProductId')->unsigned();
              $table->foreignId('ProductId')->references('id')->on('addpawns')->cascadeOnDelete();
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('Address');
            $table->integer('Phone');
            $table->string('Reference');
            $table->integer('AdharCard');
            $table->integer('PanCard');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
